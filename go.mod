module test/mergeImage

go 1.15

require (
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/ozankasikci/go-image-merge v0.2.2
)
