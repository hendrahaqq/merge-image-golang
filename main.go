package main

import (
	"errors"
	"fmt"
	"image"
	"image/jpeg"
	"image/png"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/nfnt/resize"
	gim "github.com/ozankasikci/go-image-merge"
)

func main() {

	twibbon, _ := resizeImage("https://storage.googleapis.com/assets.sekolahdesain.id/twibbon/twibbon_autocad.png", "twibbon", "d9ac62b0-5e85-4db4-a13f-7ff27a877c96")
	profile, _ := resizeImage("https://storage.googleapis.com/assets.sekolahdesain.id/user-profile/user-165-b3nhlbvkmo_profile_photo_1612234639.png", "profile", "d9ac62b0-5e85-4db4-a13f-7ff27a877c96")

	grids := []*gim.Grid{
		{
			ImageFilePath: profile,
			Grids: []*gim.Grid{
				{
					ImageFilePath: twibbon,
					OffsetX:       0, OffsetY: 0,
				},
			},
		},
	}

	// merge the images into a 2x1 grid
	rgba, err := gim.New(grids, 1, 1).Merge()
	if err != nil {
		fmt.Println(err)
	}
	// save the output to jpg or png
	file, err := os.Create("result.jpg")
	if err != nil {
		fmt.Println(err)
	}
	err = jpeg.Encode(file, rgba, &jpeg.Options{Quality: 100})

	err = os.Remove(twibbon)
	if err != nil {
		fmt.Println(err)
		return
	}
	err = os.Remove(profile)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func downloadFile(URL, fileName string) (string, error) {
	//Get the response bytes from the url
	response, err := http.Get(URL)
	if err != nil {
		return "", err
	}
	defer response.Body.Close()

	if response.StatusCode != 200 {
		return "", errors.New("Received non 200 response code")
	}
	//Create a empty file
	file, err := os.Create(fileName)
	if err != nil {
		return "", err
	}
	defer file.Close()

	//Write the bytes to the fiel
	_, err = io.Copy(file, response.Body)
	if err != nil {
		return "", err
	}

	return fileName, nil
}

func resizeImage(url, fileStatus, owner string) (string, error) {
	var (
		err            error
		img            image.Image
		file, fileName string
	)

	name := fileStatus + "-" + owner

	if url != "" {
		file, err = downloadFile(url, name)
		if err != nil {
			return "", err
		}

	} else {
		file = "icon.png"
	}
	imgFile, err := os.Open(file)
	if err != nil {
		log.Println("error 1", err)
		return "", err
	}

	img, _, err = image.Decode(imgFile)
	if err != nil {
		log.Println(err)
		return "", err
	}

	imgFile.Close()

	// decode image into image.Image
	image := resize.Resize(200, 200, img, resize.Lanczos3)
	fileName = name + ".png"
	output1, err := os.Create(fileName)
	if err != nil {
		log.Println(err)
		return "", err
	}
	defer output1.Close()
	// write new image to file
	png.Encode(output1, image)

	if url != "" {
		err = os.Remove(file)
		if err != nil {
			fmt.Println(err)
			return "", err
		}
	}

	return fileName, nil
}
